from random import randrange

import pytest
from telegram import User

from tests.unit.conftest import FIRST_NAME, USERNAME


@pytest.fixture()
def user() -> User:
    return User(id=randrange(10), first_name=FIRST_NAME, is_bot=False, username=USERNAME)
