import datetime

import pytest
from faker import Faker

from app.internal.models import TelegramUser
from app.internal.services.jwt_token_generator import JWTGenerator
from app.internal.services.storage.user_storage import UserStorage

_faker = Faker()

TELEGRAM_ID = "12346"
FIRST_NAME = "cool"
USERNAME = "unique username"
PASSWORD = _faker.word()
DATE = datetime.date(2007, 2, 6)


@pytest.fixture()
def db_user():
    return TelegramUser(telegram_id=TELEGRAM_ID, first_name=FIRST_NAME, username=USERNAME)


@pytest.fixture()
def db_user_with_password(db_user: TelegramUser):
    db_user.set_password(PASSWORD)
    return db_user


@pytest.fixture()
def user_storage(mocker):
    return mocker.create_autospec(UserStorage)


@pytest.fixture()
def jwt_generator(mocker):
    return mocker.create_autospec(JWTGenerator)
