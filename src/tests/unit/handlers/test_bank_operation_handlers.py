import pytest

from app.internal.deps import bot_manager
from app.internal.services.validation import ErrorMessage


@pytest.mark.usefixtures("mocked_db_user")
class TestBankOperationHandlers:
    @pytest.mark.parametrize(
        ("response", "expected"), [(True, "Успешно перевел!"), (False, ErrorMessage.UNKNOWN_ERROR.value)]
    )
    def test_send(self, monkeypatch, response, expected, update, context):
        pass
