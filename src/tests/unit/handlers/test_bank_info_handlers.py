import pytest

from app.internal.deps import bot_manager
from app.internal.services.models.bank_info import BankAccountModel, BankCardModel, BankInfo
from app.internal.services.models.user_info import TelegramUserModel
from app.internal.transport.bot.handlers import accounts_info
from tests.unit.conftest import DATE
from tests.unit.utils import mocked_manager_handler


@pytest.mark.usefixtures("mocked_db_user")
class TestBankInfoHandlers:
    @pytest.mark.parametrize(
        ("response", "has_cards", "expected"),
        [
            (
                BankInfo(
                    account=BankAccountModel(
                        number="123",
                        amount=0,
                        client=TelegramUserModel(telegram_id="1", first_name="name", username="username"),
                    )
                ),
                False,
                "Номер счета: 123\nСумма на счету: 0.0",
            ),
            (
                BankInfo(
                    account=BankAccountModel(
                        number="124",
                        amount=5.4,
                        client=TelegramUserModel(telegram_id="1", first_name="name", username="username"),
                    ),
                ),
                True,
                "Номер счета: 124\nСумма на счету: 5.4\nКоличество карт, привязанных к счету: 1.\n Номер карты: 123\nДата истечения действия карты: 2007-02-06\n",
            ),
            (BankInfo(), False, "У тебя нет счетов"),
        ],
    )
    def test_acounts_info(self, monkeypatch, update, context, response, has_cards, expected):
        if has_cards:
            response.cards = [
                BankCardModel(number="123", expiration_date=DATE, security_code=345, account=response.account)
            ]
        monkeypatch.setattr(bot_manager, "get_account_info", mocked_manager_handler(response))

        accounts_info(update, context)

        update.message.reply_text.assert_called_once_with(expected)
