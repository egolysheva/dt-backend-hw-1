import pytest

from app.internal.deps import bot_manager
from app.internal.services.models.favorites_info import FavoritesErrorStatus, FavoritesInfo, FavoritesStatus
from app.internal.services.models.user_info import TelegramUserModel
from app.internal.services.validation import ErrorAdvice, ErrorMessage
from app.internal.transport.bot.handlers import add_favorites, delete_favorites, favorites
from tests.unit.utils import mocked_manager_handler


@pytest.mark.usefixtures("mocked_db_user")
class TestFavoritesHandlers:
    @pytest.mark.parametrize(
        ("response", "expected"),
        [
            (
                FavoritesInfo(
                    status=FavoritesStatus.FIND_FAVORITES,
                    result=[
                        TelegramUserModel(telegram_id="56", first_name="liza", username="lizok"),
                        TelegramUserModel(telegram_id="57", first_name="alyona", username="alya"),
                    ],
                ),
                "Твои избранные пользователи: lizok; alya",
            ),
            (
                FavoritesInfo(status=FavoritesStatus.NOT_FAVORITES),
                "У тебя нет избранных пользователей. Чтобы добавить, воспользуйся командой /add.",
            ),
        ],
    )
    def test_favorites(self, monkeypatch, response, expected, update, context):
        monkeypatch.setattr(bot_manager, "get_favorites_info", mocked_manager_handler(response))

        favorites(update, context)

        update.message.reply_text.assert_called_once_with(expected)

    @pytest.mark.parametrize(
        ("response", "expected"),
        [
            (
                FavoritesInfo(status=FavoritesErrorStatus.MISSING_ARGUMENTS),
                ErrorMessage.COMMAND_WITHOUT_ARGS.value + ErrorAdvice.FAVORITES_ADVICE.value,
            ),
            (
                FavoritesInfo(status=FavoritesErrorStatus.NOT_ALLOWED_SELF),
                "Нельзя так сильно себя любить и добавлять себя в избранное!",
            ),
            (
                FavoritesInfo(
                    status=FavoritesStatus.ADD_FAVORITES,
                    result=[
                        TelegramUserModel(telegram_id="59", first_name="vanya", username="vano"),
                        TelegramUserModel(telegram_id="51", first_name="sasha", username="alex"),
                    ],
                ),
                "Добавил!\nТвои избранные пользователи: vano; alex",
            ),
            (
                FavoritesInfo(
                    status=FavoritesStatus.NOT_FAVORITES,
                    result=[
                        TelegramUserModel(telegram_id="59", first_name="vanya", username="vano"),
                        TelegramUserModel(telegram_id="51", first_name="sasha", username="alex"),
                    ],
                ),
                "Не удалось добавить пользователей vano; alex из избранного, потому что их не существует!\n",
            ),
        ],
    )
    def test_add_favorites(self, monkeypatch, response, expected, update, context):
        monkeypatch.setattr(bot_manager, "add_favorites", mocked_manager_handler(response))

        add_favorites(update, context)

        update.message.reply_text.assert_called_once_with(expected)

    @pytest.mark.parametrize(
        ("response", "expected"),
        [
            (
                FavoritesInfo(status=FavoritesErrorStatus.MISSING_ARGUMENTS),
                ErrorMessage.COMMAND_WITHOUT_ARGS.value + ErrorAdvice.FAVORITES_ADVICE.value,
            ),
            (
                FavoritesInfo(
                    status=FavoritesStatus.DELETE_FAVORITES,
                    result=[
                        TelegramUserModel(telegram_id="59", first_name="vanya", username="vano"),
                        TelegramUserModel(telegram_id="51", first_name="sasha", username="alex"),
                    ],
                ),
                "Удалил!\nТвои избранные пользователи: vano; alex",
            ),
            (
                FavoritesInfo(
                    status=FavoritesStatus.NOT_FAVORITES,
                    result=[
                        TelegramUserModel(telegram_id="59", first_name="vanya", username="vano"),
                        TelegramUserModel(telegram_id="51", first_name="sasha", username="alex"),
                    ],
                ),
                "Не удалось удалить пользователей vano; alex из избранного, потому что их не существует!\n",
            ),
        ],
    )
    def test_delete_favorites(self, monkeypatch, response, expected, update, context):
        monkeypatch.setattr(bot_manager, "delete_favorites", mocked_manager_handler(response))

        delete_favorites(update, context)

        update.message.reply_text.assert_called_once_with(expected)
