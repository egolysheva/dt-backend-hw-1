import pytest

from app.internal.deps import bot_manager
from app.internal.services.models.user_info import PhoneStatus, TelegramUserModel, TelegramUserResponse, UserStatus
from app.internal.transport.bot.handlers import me, set_phone, start
from tests.unit.conftest import USERNAME
from tests.unit.utils import mocked_manager_handler


@pytest.mark.usefixtures("mocked_db_user")
class TestUserInfoHandlers:
    @pytest.mark.parametrize(
        ("response", "expected"),
        [
            (
                TelegramUserResponse(
                    status=UserStatus.NEW,
                    user=TelegramUserModel(telegram_id=12344567, username="collest", first_name="alex"),
                ),
                "Бонжур, collest!",
            ),
            (
                TelegramUserResponse(
                    status=UserStatus.OLD,
                    user=TelegramUserModel(telegram_id=12344568, username="smart", first_name="anfica"),
                ),
                "И снова здравствуй, smart!",
            ),
        ],
    )
    def test_start(self, monkeypatch, response, update, context, expected):
        monkeypatch.setattr(bot_manager, "start", mocked_manager_handler(response))

        start(update=update, context=context)

        update.message.reply_text.assert_called_once_with(expected)

    @pytest.mark.parametrize(
        ("response", "expected"),
        [
            (PhoneStatus.NOT_CORRECT_PHONE, "Ты ввел мне не телефон :( \nПожалуйста, соответсвуй формату +79123456789"),
            (PhoneStatus.UPDATE_PHONE, f"{USERNAME}, я обновил твой номер телефона!"),
        ],
    )
    def test_set_phone(self, monkeypatch, response, expected, update, context, db_user):
        monkeypatch.setattr(bot_manager, "update_phone", mocked_manager_handler(response))

        set_phone(update, context)

        update.message.reply_text.assert_called_once_with(expected)

    def test_me(self, update, context, db_user):
        user_info = (
            f"Твое имя: {db_user.first_name} {db_user.last_name if db_user.last_name else ''}\n"
            + f"Твой id: {db_user.telegram_id}\n"
            + f"Твой номер телефона: {db_user.phone if db_user.phone else 'не установлен'}\n"
            + f"Твой никнейм: {db_user.username}\n"
            + "А что ты знаешь обо мне? :D"
        )

        me(update, context)

        update.message.reply_text.assert_called_once_with(user_info)
