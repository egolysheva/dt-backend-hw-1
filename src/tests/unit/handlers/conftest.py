import datetime
from unittest import mock

import pytest
from faker import Faker
from telegram import User

from app.internal.deps import bot_manager
from tests.unit.utils import mocked_manager_handler


@pytest.fixture()
def context() -> mock.MagicMock:
    return mock.MagicMock()


@pytest.fixture()
def message(user: User):
    message = mock.MagicMock()
    message.from_user.return_value = user
    message.reply_text = mock.MagicMock()

    return message


@pytest.fixture()
def update(user, message):
    update = mock.MagicMock()
    update.message = message
    update.effective_user.return_value = user

    return update


@pytest.fixture
def mocked_db_user(monkeypatch, db_user):
    monkeypatch.setattr(bot_manager._user_storage, "get_user", mocked_manager_handler(db_user))
