import pytest

from app.internal.models import TelegramUser
from app.internal.services.bot_manager import BotManager, _convert_db_user_to_user_model
from app.internal.services.models.bank_info import TransactionInfoStatus
from app.internal.services.models.user_info import TelegramUserResponse, UserStatus


@pytest.fixture
def bot_manager(user_storage, favorites_storage, bank_storage):
    return BotManager(user_storage=user_storage, favorites_storage=favorites_storage, bank_storage=bank_storage)


@pytest.mark.django_db
class TestBotManager:
    def test_start_when_user_not_exists(self, bot_manager, user):
        actual = bot_manager.start(user)
        db_user = TelegramUser.objects.get(telegram_id=user.id)

        assert actual == TelegramUserResponse(status=UserStatus.NEW, user=_convert_db_user_to_user_model(db_user))

    def test_get_transactions_when_exists(self, bot_manager, test_user_with_account, create_transactions):
        response = bot_manager.get_transactions(test_user_with_account)

        assert response.status == TransactionInfoStatus.FIND_TRANSACTION
        assert response.transactions
        for tr in response.transactions:
            assert test_user_with_account.username in [tr.from_, tr.to]

    def test_get_users(self, bot_manager, test_user_with_account, second_test_user_with_account, create_transactions):
        response = bot_manager.get_users(test_user_with_account)
        assert len(response) == 1
        assert second_test_user_with_account.username in response
