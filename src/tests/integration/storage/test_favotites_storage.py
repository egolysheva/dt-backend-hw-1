import pytest

from app.internal.models import TelegramUser
from app.internal.services.models.user_info import TelegramUserModel


@pytest.mark.django_db
class TestFavoritesStorage:
    @pytest.mark.parametrize(
        "add_user_in_database",
        [
            [
                TelegramUserModel(telegram_id="12", first_name="lol", username="lola"),
                TelegramUserModel(telegram_id="11", first_name="kek", username="keka"),
            ]
        ],
        indirect=True,
    )
    def test_add_favorites(self, favorites_storage, user_storage, user, add_user_in_database):
        user_storage.create_user(user)
        db_user = user_storage.get_user(user.id)

        favorites_storage.add_favorites(db_user, ["lola", "vasya", "keka"])

        new_usernames = [
            username
            for username in TelegramUser.objects.get(telegram_id=user.id).favorites.values_list("username", flat=True)
        ]

        for name in ["lola", "keka"]:
            assert name in new_usernames
        assert "vasya" not in new_usernames

    @pytest.mark.parametrize(
        "add_user_in_database",
        [
            [
                TelegramUserModel(telegram_id="12", first_name="lol", username="lola"),
                TelegramUserModel(telegram_id="11", first_name="kek", username="keka"),
            ]
        ],
        indirect=True,
    )
    def test_delete_favorites(self, favorites_storage, user_storage, user, add_user_in_database):
        user_storage.create_user(user)
        db_user = user_storage.get_user(user.id)
        favorites_storage.add_favorites(db_user, ["lola", "vasya", "keka"])

        favorites_storage.delete_favorites(db_user, ["lola", "liza"])

        new_usernames = [
            username
            for username in TelegramUser.objects.get(telegram_id=user.id).favorites.values_list("username", flat=True)
        ]

        assert len(new_usernames) == 1
        assert new_usernames[0] == "keka"
