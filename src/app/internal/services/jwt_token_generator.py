from datetime import datetime, timedelta
from typing import Optional

import jwt
import pytz
from pydantic.main import BaseModel

from app.internal.models import TelegramUser
from app.internal.models.token import Token
from app.internal.services.storage.token_storage import TokenStorage
from config import settings


class LoginResponse(BaseModel):
    access_token: str
    refresh_token: str


class JWTGenerator:
    def __init__(self, token_storage: TokenStorage):
        self._storage = token_storage

    def generate_access_and_refresh_tokens(self, user: TelegramUser) -> LoginResponse:
        access_token = self._generate_access_token(user.telegram_id)
        refresh_token = self._generate_refresh_token()

        self._storage.create_token(user, refresh_token)

        return LoginResponse(access_token=access_token, refresh_token=refresh_token)

    def get_token(self, refresh_token: str) -> Optional[Token]:
        return self._storage.get_token(refresh_token)

    def has_token_expired(self, refresh_token: str) -> bool:
        try:
            payload = jwt.decode(refresh_token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.InvalidTokenError as ex:
            print(ex)
            return True

        expired = payload.get("expired")

        if expired is None:
            return True

        return datetime.fromtimestamp(expired, tz=pytz.UTC) < datetime.now()

    def get_telegram_id(self, token: str) -> int | None:
        try:
            payload = jwt.decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.InvalidTokenError as ex:
            print(ex)
            return None

        return payload.get("id")

    def _generate_access_token(self, telegram_id: int) -> str:
        dt = datetime.now() + timedelta(minutes=settings.JWT_ACCESS_TOKEN_EXP_TIME)

        return jwt.encode(
            {"id": telegram_id, "expired": int(dt.timestamp())}, settings.JWT_SECRET_KEY, algorithm="HS256"
        )

    def _generate_refresh_token(self) -> str:
        dt = datetime.now() + timedelta(days=settings.JWT_REFRESH_TOKEN_EXP_DAY)

        return jwt.encode({"expired": int(dt.timestamp())}, settings.JWT_SECRET_KEY, algorithm="HS256")
