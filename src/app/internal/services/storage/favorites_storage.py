from app.internal.models import TelegramUser


class FavoritesStorage:
    def delete_favorites(self, user: TelegramUser, usernames: list[str]) -> list[TelegramUser]:
        new_favotite_users = (
            TelegramUser.objects.get(telegram_id=user.telegram_id).favorites.exclude(username__in=usernames).all()
        )
        user.favorites.set(new_favotite_users)
        user.save()

        return new_favotite_users

    def add_favorites(self, user: TelegramUser, usernames: list[str]) -> list[TelegramUser]:
        new_favotite_users = TelegramUser.objects.filter(username__in=usernames).all()
        user.favorites.set(user.favorites.all() | new_favotite_users)
        user.save()

        return new_favotite_users
