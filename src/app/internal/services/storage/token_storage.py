from typing import Optional

from django.db import transaction

from app.internal.models import TelegramUser
from app.internal.models.token import Token


class TokenStorage:
    @transaction.atomic()
    def create_token(self, user: TelegramUser, refresh_token: str) -> None:
        if token := self.get_token(refresh_token):
            token.jwt = refresh_token
            token.save(update_fields=["jwt"])
        else:
            Token.objects.create(jwt=refresh_token, user=user)

    def get_token(self, refresh_token: str) -> Optional[Token]:
        return Token.objects.filter(jwt=refresh_token).first()
