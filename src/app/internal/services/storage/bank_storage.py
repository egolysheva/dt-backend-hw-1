from typing import List

from django.db.models import Q

from app.internal.models.transaction import Transaction


class BankStorage:
    def _get_transactions_query(self, account_number: str):
        return Transaction.objects.filter(
            Q(to_account__number=account_number) | Q(from_account__number=account_number)
        ).order_by("-created_at")

    def get_transactions_by_account_number(self, account_number: str) -> List[Transaction]:
        return self._get_transactions_query(account_number).values(
            "transaction_amount", "from_account__client__username", "to_account__client__username", "created_at"
        )

    def get_users(self, account_number: str):
        from_users = Transaction.objects.filter(Q(to_account__number=account_number)).values_list(
            "from_account__client__username", flat=True
        )

        to_users = Transaction.objects.filter(Q(from_account__number=account_number)).values_list(
            "to_account__client__username", flat=True
        )

        users = set(from_users)
        users.update(to_users)

        return users
