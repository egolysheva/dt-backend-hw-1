import functools
import re
from enum import Enum

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.deps import bot_manager
from app.internal.models import BankAccount, TelegramUser
from app.internal.models.bank_card import ACCOUNT_NUMBER_REGEX


def account_not_exist_error_message(username: str):
    return f"У пользователя {username} нет счетов"


class ErrorMessage(Enum):
    USER_INFO_NOT_EXISTS = "Сначала заполни информацию о себе.\n"
    NOT_CORRECT_NUMBER_ARGS = "Некорректное число аргументов!\n"
    COMMAND_WITHOUT_ARGS = "Эту команду нельзя использовать без аргументов."
    UNKNOWN_ERROR = "Я сломался :(\nПопробуй позже"


class ErrorAdvice(Enum):
    FAVORITES_ADVICE = "Пожалуйста, введи username пользователей"


def validate_user_info(func):
    def wrapper(update: Update, context: CallbackContext):
        user_id = update.effective_user.id
        db_user = bot_manager._user_storage.get_user(user_id)
        if db_user is None:
            update.message.reply_text(ErrorMessage.USER_INFO_NOT_EXISTS.value + "Вызови команду /start.")
            return
        if db_user.phone is None:
            update.message.reply_text(
                ErrorMessage.USER_INFO_NOT_EXISTS.value + "Вызови команду /set_phone со своим номером телефона."
            )
            return
        return func(update, context, db_user)

    return wrapper


def parse_and_validate_send_args(func):
    def wrapper(update: Update, context: CallbackContext, from_: TelegramUser):
        if len(context.args) != 2:
            update.message.reply_text(
                ErrorMessage.NOT_CORRECT_NUMBER_ARGS.value
                + "Пожалуйста, введи username или номер счета и сумму, которую хочешь перечислить."
            )
            return

        if number := re.match(ACCOUNT_NUMBER_REGEX, context.args[0]):

            to_account = BankAccount.objects.filter(number=number.group(0))

            if len(to_account) == 0:
                update.message.reply_text(f"Cчета {number.group(0)} не существует!")
                return

        else:
            to = bot_manager.user_storage.get_user_by_name(context.args[0])
            if not to:
                update.message.reply_text("Пользователя, которому ты хочешь перевести деньги, не существует!")
                return
            to_account = BankAccount.objects.filter(client=to)

            if len(to_account) == 0:
                update.message.reply_text(account_not_exist_error_message(to.username))
                return

        if to_account[0].client.telegram_id == from_.telegram_id:
            update.message.reply_text("Нельзя отправлять деньги самому себе!")
            return

        if not context.args[1].isdigit():
            update.message.reply_text("Вторым аргументом нужно ввести целое положительное число!")
            return

        count = int(context.args[1])
        from_account = BankAccount.objects.filter(client=from_)
        if len(from_account) == 0:
            update.message.reply_text(account_not_exist_error_message(from_.username))
            return

        if from_account[0].amount < count:
            update.message.reply_text("У тебя недостаточно денег на счету")
            return

        return func(update, from_account[0], to_account[0], count)

    return wrapper
