import re

from django.db import transaction
from pydantic.types import SecretStr
from telegram import User
from telegram.ext import CallbackContext

from app.internal.models import BankAccount, BankCard, TelegramUser
from app.internal.models.transaction import Transaction
from app.internal.services.models.bank_info import (
    BankAccountModel,
    BankCardModel,
    BankInfo,
    TransactionInfo,
    TransactionInfoStatus,
    TransactionModel,
)
from app.internal.services.models.favorites_info import (
    FavoritesErrorStatus,
    FavoritesInfo,
    FavoritesResponseStatus,
    FavoritesStatus,
)
from app.internal.services.models.user_info import PhoneStatus, TelegramUserModel, TelegramUserResponse, UserStatus
from app.internal.services.storage.bank_storage import BankStorage
from app.internal.services.storage.favorites_storage import FavoritesStorage
from app.internal.services.storage.user_storage import UserStorage

PHONE_REGEX = r"^\+79\d{9}$"
PHONE_REGEX_PATTERN = re.compile(PHONE_REGEX)


def _convert_db_user_to_user_model(user: TelegramUser):
    return TelegramUserModel(
        telegram_id=user.telegram_id,
        first_name=user.first_name,
        last_name=user.last_name,
        username=user.username,
        phone=user.phone,
        favorites=list(user.favorites.all()),
    )


class BotManager:
    def __init__(self, user_storage: UserStorage, favorites_storage: FavoritesStorage, bank_storage: BankStorage):
        self._user_storage = user_storage
        self._favorites_storage = favorites_storage
        self._bank_storage = bank_storage

    def start(self, user: User) -> TelegramUserResponse:
        if db_user := self._user_storage.get_user(user.id):
            TelegramUserResponse(status=UserStatus.OLD, user=_convert_db_user_to_user_model(db_user))

        new_user = self._user_storage.create_user(user)
        return TelegramUserResponse(status=UserStatus.NEW, user=_convert_db_user_to_user_model(new_user))

    def update_phone(self, context: CallbackContext, db_user: TelegramUser) -> PhoneStatus:
        if len(context.args) != 1:
            return PhoneStatus.NOT_CORRECT_PHONE

        phone = context.args[0]
        if not PHONE_REGEX_PATTERN.fullmatch(phone):
            return PhoneStatus.NOT_CORRECT_PHONE

        self._user_storage.set_phone(db_user, phone)
        return PhoneStatus.UPDATE_PHONE

    def get_account_info(user: TelegramUser) -> BankInfo:
        bank_info = BankInfo()
        if account := BankAccount.objects.filter(client=user):
            bank_info.account = BankAccountModel.from_orm(account)
            if cards := BankCard.objects.filter(account=account):
                bank_info.cards = [BankCardModel.from_orm(card) for card in cards]

        return bank_info

    def get_favorites_info(self, user: TelegramUser) -> FavoritesInfo:
        if favorites := user.favorites.all():
            return FavoritesInfo(status=FavoritesResponseStatus.FIND_FAVORITES, result=list(favorites))

        return FavoritesInfo(status=FavoritesResponseStatus.NOT_FAVORITES)

    def _get_not_exists_users(self, new_favotite_users: list[TelegramUser], usernames: list[str]) -> list[str]:
        exist_users = [user.username for user in new_favotite_users]
        not_exist_users = [username for username in usernames if username not in exist_users]

        return not_exist_users

    def add_favorites(self, user: TelegramUser, context: CallbackContext) -> FavoritesInfo:
        if not context.args:
            return FavoritesInfo(status=FavoritesErrorStatus.MISSING_ARGUMENTS)

        if user.username in context.args:
            return FavoritesInfo(status=FavoritesErrorStatus.NOT_ALLOWED_SELF)

        usernames = context.args
        new_favorites = self._favorites_storage.add_favorites(user, usernames)
        not_exist_users = self._get_not_exists_users(new_favorites, usernames)
        if not_exist_users:
            return FavoritesInfo(status=FavoritesStatus.NOT_FAVORITES, result=not_exist_users)

        return FavoritesInfo(status=FavoritesStatus.ADD_FAVORITES, result=self.get_favorites_info(user).result)

    def delete_favorites(self, user: TelegramUser, context: CallbackContext) -> FavoritesInfo:
        if not context.args:
            return FavoritesInfo(status=FavoritesErrorStatus.MISSING_ARGUMENTS)

        usernames = context.args
        new_favorites = self._favorites_storage.delete_favorites(user, usernames)
        not_exist_users = self._get_not_exists_users(new_favorites, usernames)

        if not_exist_users:
            return FavoritesInfo(status=FavoritesResponseStatus.NOT_FAVORITES, result=not_exist_users)

        return FavoritesInfo(
            status=FavoritesResponseStatus.DELETE_FAVORITES, result=self.get_favorites_info(user).result
        )

    @transaction.atomic()
    def send_money(self, from_account: BankAccount, to_account: BankAccount, count: int) -> bool:
        try:
            from_account.amount = from_account.amount - count
            from_account.save(update_fields=["amount"])

            to_account.amount = to_account.amount + count
            to_account.save(update_fields=["amount"])
        except Exception:
            return False

        return True

    def get_transactions(self, user: TelegramUser) -> TransactionInfo:
        if account_number := self._user_storage.get_account_number(user):
            if transactions := self._bank_storage.get_transactions_by_account_number(account_number):
                return TransactionInfo(
                    status=TransactionInfoStatus.FIND_TRANSACTION,
                    transactions=[
                        TransactionModel(
                            from_=tr["from_account__client__username"],
                            to=tr["to_account__client__username"],
                            amount=tr["transaction_amount"],
                            created_at=tr["created_at"],
                        )
                        for tr in transactions
                    ],
                )

            return TransactionInfo(status=TransactionInfoStatus.NOT_TRANSACTION)

        return TransactionInfo(status=TransactionInfoStatus.NOT_ACCOUNT)

    def get_users(self, user: TelegramUser):
        if account_number := self._user_storage.get_account_number(user):
            return self._bank_storage.get_users(account_number)

        return []

    def set_password(self, user: TelegramUser, password: SecretStr) -> bool:
        return self._user_storage.set_password(user, password)
