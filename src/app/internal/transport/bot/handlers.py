from pydantic import SecretStr
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.deps import bot_manager
from app.internal.models import BankAccount, TelegramUser
from app.internal.services.models.bank_info import TransactionInfoStatus
from app.internal.services.models.favorites_info import FavoritesErrorStatus, FavoritesResponseStatus, FavoritesStatus
from app.internal.services.models.user_info import PhoneStatus, UserStatus
from app.internal.services.validation import ErrorAdvice, ErrorMessage, parse_and_validate_send_args, validate_user_info
from app.internal.utils import usernames_view


def start(update: Update, context: CallbackContext) -> None:
    user = update.effective_user

    response = bot_manager.start(user)
    match response.status:
        case UserStatus.NEW:
            update.message.reply_text(f"Бонжур, {response.user.username}!")
        case UserStatus.OLD:
            print("a")
            update.message.reply_text(f"И снова здравствуй, {response.user.username}!")
        case _:
            update.message.reply_text(ErrorMessage.UNKNOWN_ERROR.value)


@validate_user_info
def set_phone(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    match bot_manager.update_phone(context, db_user):
        case PhoneStatus.NOT_CORRECT_PHONE:
            update.message.reply_text("Ты ввел мне не телефон :( \nПожалуйста, соответсвуй формату +79123456789")
        case PhoneStatus.UPDATE_PHONE:
            update.message.reply_text(f"{db_user.username}, я обновил твой номер телефона!")
        case _:
            update.message.reply_text(ErrorMessage.UNKNOWN_ERROR.value)


@validate_user_info
def me(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    user_info = (
        f"Твое имя: {db_user.first_name} {db_user.last_name if db_user.last_name else ''}\n"
        + f"Твой id: {db_user.telegram_id}\n"
        + f"Твой номер телефона: {db_user.phone if db_user.phone else 'не установлен'}\n"
        + f"Твой никнейм: {db_user.username}\n"
        + "А что ты знаешь обо мне? :D"
    )

    update.message.reply_text(user_info)


@validate_user_info
def accounts_info(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    bank_info = bot_manager.get_account_info(db_user)
    if account := bank_info.account:
        info = f"Номер счета: {account.number}\nСумма на счету: {account.amount}"
        if cards := bank_info.cards:
            info += f"\nКоличество карт, привязанных к счету: {len(cards)}.\n "
            for card in cards:
                info += f"Номер карты: {card.number}\nДата истечения действия карты: {card.expiration_date}\n"
    else:
        info = "У тебя нет счетов"

    update.message.reply_text(info)


@validate_user_info
def favorites(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    response = bot_manager.get_favorites_info(db_user)
    match response.status:
        case FavoritesStatus.NOT_FAVORITES:
            update.message.reply_text("У тебя нет избранных пользователей. Чтобы добавить, воспользуйся командой /add.")
        case FavoritesStatus.FIND_FAVORITES:
            update.message.reply_text("Твои избранные пользователи: " + usernames_view(response))
        case _:
            update.message.reply_text(ErrorMessage.UNKNOWN_ERROR.value)


@validate_user_info
def add_favorites(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    response = bot_manager.add_favorites(db_user, context)
    match response.status:
        case FavoritesErrorStatus.MISSING_ARGUMENTS:
            update.message.reply_text(ErrorMessage.COMMAND_WITHOUT_ARGS.value + ErrorAdvice.FAVORITES_ADVICE.value)
        case FavoritesErrorStatus.NOT_ALLOWED_SELF:
            update.message.reply_text("Нельзя так сильно себя любить и добавлять себя в избранное!")
        case FavoritesStatus.NOT_FAVORITES:
            update.message.reply_text(
                f"Не удалось добавить пользователей {usernames_view(response)} из избранного, потому что их не существует!\n"
            )
        case FavoritesStatus.ADD_FAVORITES:
            update.message.reply_text("Добавил!\nТвои избранные пользователи: " + usernames_view(response))
        case _:
            update.message.reply_text(ErrorMessage.UNKNOWN_ERROR.value)


@validate_user_info
def delete_favorites(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    response = bot_manager.delete_favorites(db_user, context)
    match response.status:
        case FavoritesErrorStatus.MISSING_ARGUMENTS:
            update.message.reply_text(ErrorMessage.COMMAND_WITHOUT_ARGS.value + ErrorAdvice.FAVORITES_ADVICE.value)
        case FavoritesStatus.NOT_FAVORITES:
            update.message.reply_text(
                f"Не удалось удалить пользователей {usernames_view(response)} из избранного, потому что их не существует!\n"
            )
        case FavoritesStatus.DELETE_FAVORITES:
            update.message.reply_text("Удалил!\nТвои избранные пользователи: " + usernames_view(response))
        case _:
            update.message.reply_text(ErrorMessage.UNKNOWN_ERROR.value)


@validate_user_info
@parse_and_validate_send_args
def send(update: Update, from_account: BankAccount, to_account: BankAccount, count: int) -> None:
    if bot_manager.send_money(from_account, to_account, count):
        update.message.reply_text("Успешно перевел!")
    else:
        update.message.reply_text(ErrorMessage.UNKNOWN_ERROR.value)


@validate_user_info
def transactions(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    response = bot_manager.get_transactions(db_user)
    match response.status:
        case TransactionInfoStatus.NOT_ACCOUNT:
            update.message.reply_text("У тебя нет счетов")
        case TransactionInfoStatus.NOT_TRANSACTION:
            update.message.reply_text("Нет информации о совершенных транзакциях")
        case TransactionInfoStatus.FIND_TRANSACTION:
            data = []
            for transaction in response.transactions:
                data.append(f"от: {transaction.from_}")
                data.append(f"кому: {transaction.to}")
                data.append(f"сумма: {transaction.amount}")
                data.append(f"когда: {transaction.created_at}")
                data.append("---------------------------")
                update.message.reply_text("\n".join(data))
        case _:
            update.message.reply_text(ErrorMessage.UNKNOWN_ERROR.value)


@validate_user_info
def users(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    response = bot_manager.get_users(db_user)
    update.message.reply_text("Пользователи, с которыми у тебя были денеждные дела:" + "; ".join(response))


@validate_user_info
def set_password(update: Update, context: CallbackContext, db_user: TelegramUser) -> None:
    if len(context.args) != 1:
        update.message.reply_text("Вызови команду в формате /set_password password")
        return

    if bot_manager.set_password(db_user, SecretStr(context.args[0])):
        update.message.reply_text("Обновил твой пароль")
    else:
        update.message.reply_text("Что то пошло не так, попробуй позже :(")
