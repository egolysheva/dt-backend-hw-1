from typing import Union

from django.http import HttpRequest, HttpResponseNotFound, JsonResponse
from django.views import View

from app.internal.bot import BOT


class TelegramBotView(View):
    def post(self, request: HttpRequest) -> Union[JsonResponse, HttpResponseNotFound]:
        BOT.update(request.body)
        return JsonResponse(data={"OK": "POST request processed"})
