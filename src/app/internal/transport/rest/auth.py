from http import HTTPStatus
from typing import Any, Optional

from django.http import HttpRequest, JsonResponse
from ninja.security import HttpBearer

from app.internal.deps import jwt_generator


class Authentication(HttpBearer):
    def authenticate(self, request: HttpRequest, token: str) -> Optional[Any]:
        if jwt_generator.has_token_expired(token):
            return None

        return token
