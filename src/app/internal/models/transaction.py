from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import SET_NULL

from app.internal.models import BankAccount


class Transaction(models.Model):
    from_account = models.ForeignKey(to=BankAccount, on_delete=SET_NULL, null=True, related_name="transactions_from")
    to_account = models.ForeignKey(to=BankAccount, on_delete=SET_NULL, null=True, related_name="transactions_to")
    transaction_amount = models.DecimalField(
        validators=[MinValueValidator(limit_value=0)], max_digits=10, decimal_places=2
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "transaction"
