from django.db import models

from app.internal.models import TelegramUser


class Token(models.Model):
    jwt = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey(TelegramUser, related_name="tokens", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)

    objects = models.Manager()
