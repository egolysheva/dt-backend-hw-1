from django.core.validators import MinValueValidator, RegexValidator
from django.db import models

from .telegram_user import TelegramUser

ACCOUNT_NUMBER_REGEX = r"^\d{20}$"
CARD_NUMBER_REGEX = r"^\d{16}$"
CARD_SECURITY_CODE_REGEX = r"^\d{3}$"


class BankAccount(models.Model):
    client = models.OneToOneField(to=TelegramUser, on_delete=models.CASCADE, related_name="account")
    amount = models.DecimalField(
        default=0, validators=[MinValueValidator(limit_value=0)], max_digits=10, decimal_places=2
    )
    number = models.CharField(
        db_index=True,
        max_length=20,
        primary_key=True,
        validators=[
            RegexValidator(regex=ACCOUNT_NUMBER_REGEX, message="Account number must be a twenty-digit number!")
        ],
    )

    def __str__(self):
        return f"Bank account: {self.number}"

    class Meta:
        db_table = "bank_account"


class BankCard(models.Model):
    number = models.CharField(
        db_index=True,
        max_length=16,
        primary_key=True,
        validators=[RegexValidator(regex=CARD_NUMBER_REGEX, message="Card number must be a sixteen-digit number!")],
    )
    expiration_date = models.DateField()
    security_code = models.IntegerField(
        validators=[
            RegexValidator(regex=CARD_SECURITY_CODE_REGEX, message="Card security_code must match the format: 000")
        ]
    )
    account = models.ForeignKey(to=BankAccount, on_delete=models.CASCADE)

    def __str__(self):
        return f"Bank card: {self.number}"

    class Meta:
        db_table = "bank_card"
