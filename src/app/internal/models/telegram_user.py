from django.contrib.auth.base_user import AbstractBaseUser
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

PHONE_REGEX = r"^\+79\d{9}$"


class TelegramUser(AbstractBaseUser):
    telegram_id = models.BigIntegerField(primary_key=True, db_index=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200, blank=True, null=True)
    username = models.CharField(max_length=200, unique=True)
    phone = models.TextField(
        validators=[RegexValidator(regex=PHONE_REGEX, message="Phone number must match the format: +7|89123456789!")],
        blank=True,
        max_length=12,
    )
    favorites = models.ManyToManyField(to="self", symmetrical=False, blank=True)
    password = models.CharField(_("password"), max_length=128, null=True)

    def __str__(self):
        return f"User: {self.username}"

    class Meta:
        db_table = "tg_user"
