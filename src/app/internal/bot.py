import json

import telegram
from telegram.ext import CommandHandler, Dispatcher, Updater

from app.internal.deps import bot_manager
from app.internal.transport.bot.handlers import (
    accounts_info,
    add_favorites,
    delete_favorites,
    favorites,
    me,
    send,
    set_password,
    set_phone,
    start,
    transactions,
    users,
)
from config.settings import TELEGRAM_TOKEN

COMMAND_MAPPING = {
    "start": start,
    "set_phone": set_phone,
    "me": me,
    "accounts_info": accounts_info,
    "favorites": favorites,
    "add": add_favorites,
    "delete": delete_favorites,
    "send": send,
    "transactions": transactions,
    "users": users,
    "set_password": set_password,
}


# polling
def start_bot():
    updater = Updater(token=TELEGRAM_TOKEN, use_context=True)

    dispatcher: Dispatcher = updater.dispatcher
    for name, handler in COMMAND_MAPPING.items():
        dispatcher.add_handler(CommandHandler(name, handler))
    updater.start_polling()
    updater.idle()


# webhook
class TelegramBot:
    def __init__(self, token: str, bot_manager):
        self._bot = telegram.Bot(token=token)
        self._dispatcher = Dispatcher(self._bot, None, workers=0)
        self._bot_manager = bot_manager

        for name, handler in COMMAND_MAPPING.items():
            self._dispatcher.add_handler(CommandHandler(name, handler))

    def update(self, text: str):
        update = telegram.Update.de_json(json.loads(text), self._bot)
        self._dispatcher.process_update(update)


BOT = TelegramBot(token=TELEGRAM_TOKEN, bot_manager=bot_manager)
