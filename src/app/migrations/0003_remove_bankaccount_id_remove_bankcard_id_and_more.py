# Generated by Django 4.0.3 on 2022-04-11 21:25

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0002_bankaccount_telegramuser_bankcard_bankaccount_client"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="bankaccount",
            name="id",
        ),
        migrations.RemoveField(
            model_name="bankcard",
            name="id",
        ),
        migrations.AddField(
            model_name="telegramuser",
            name="favorites",
            field=models.ManyToManyField(blank=True, to="app.telegramuser"),
        ),
        migrations.AlterField(
            model_name="bankaccount",
            name="number",
            field=models.CharField(
                db_index=True,
                max_length=20,
                primary_key=True,
                serialize=False,
                validators=[
                    django.core.validators.RegexValidator(
                        message="Account number must be a twenty-digit number!", regex="^\\d{20}$"
                    )
                ],
            ),
        ),
        migrations.AlterField(
            model_name="bankcard",
            name="number",
            field=models.CharField(
                db_index=True,
                max_length=16,
                primary_key=True,
                serialize=False,
                validators=[
                    django.core.validators.RegexValidator(
                        message="Card number must be a sixteen-digit number!", regex="^\\d{16}$"
                    )
                ],
            ),
        ),
        migrations.AlterField(
            model_name="telegramuser",
            name="username",
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
