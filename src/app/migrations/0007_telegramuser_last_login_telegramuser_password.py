# Generated by Django 4.0.4 on 2022-05-26 11:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0006_alter_transaction_created_at_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="telegramuser",
            name="last_login",
            field=models.DateTimeField(blank=True, null=True, verbose_name="last login"),
        ),
        migrations.AddField(
            model_name="telegramuser",
            name="password",
            field=models.CharField(max_length=128, null=True, verbose_name="password"),
        ),
    ]
