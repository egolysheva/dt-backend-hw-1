run_migrate:
	python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

dev:
	python src/manage.py runserver localhost:8000

start_bot:
	python src/manage.py start_bot

migrate:
	docker-compose exec web pipenv run python src/manage.py migrate $(if $m, api $m,)

createsuperuser:
	docker-compose exec web pipenv run python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

command:
	docker-compose exec web python src/manage.py ${c}

shell:
	docker-compose exec web python src/manage.py shell

debug:
	docker-compose exec web python src/manage.py debug

piplock:
	docker-compose exec web pipenv install && sudo chown -R ${USER} Pipfile.lock
	docker-compose exec bot pipenv install && sudo chown -R ${USER} Pipfile.lock

lint:
	docker-compose run web pipenv run isort .
	docker-compose run web pipenv run flake8 --config setup.cfg
	docker-compose run web pipenv run black --target-version py310 --config pyproject.toml .

lint-local:
	isort .
	flake8 --config setup.cfg
	black --target-version py310 --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --target-version py310 --config pyproject.toml .

up:
	docker-compose down -v
	docker-compose up -d

build:
	docker-compose build

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

test:
	docker-compose run web pipenv run pytest src/tests
